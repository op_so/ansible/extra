# Ansible Docker role

A role to install and configure Docker.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-informational?logo=ansible&style=flat-square)](https://galaxy.ansible.com/jfx/extra) Ansible Galaxy collection.

## Getting Started

### Requirements

In order to use:

* Minimal Ansible version: 2.10

### Dependencies

* `ansible.posix.sysctl` role for TCP forwarding configuration.

### Installation

* Download the `jfx.extra` collection:

```shell
ansible-galaxy collection install jfx.extra
```

* Then use the role from the collection in the playbook:

example:

```yaml
  ...

  roles:
    - role: jfx.extra.docker
      vars:
        docker_arch: "{{ arch }}"
          ...
```

### Docker role variables

| Variables             | Description                                         | Default             |
| --------------------- | --------------------------------------------------- | ------------------- |
| `docker_arch`         | Binary architecture of Docker: `amd64`, `arm64` ... | `amd64`             |
| `docker_keyrings_dir` | Apt keyrings directory path.                        | `/etc/apt/keyrings` |
| `docker_user`         | Add a user to Docker group.                         | null                |

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
