# Ansible nginx role

A role that downloads and installs a nginx server.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-informational?logo=ansible&style=flat-square)](https://galaxy.ansible.com/jfx/extra) Ansible Galaxy collection.

## Getting Started

### Requirements

In order to use:

* Minimal Ansible version: 2.10

### Installation

* Download the `jfx.extra` collection:

```shell
ansible-galaxy collection install jfx.extra
```

* Then use the role from the collection in the playbook:

example:

```yaml
  ...

  roles:
    - role: jfx.extra.nginx
      vars:
        nginx_sites:
          - my-site
        nginx_ssl_fqdn:
          - my-site.example.com
          ...
```

### nginx role variables

| Variables             | Description                                                          | Default            |
| --------------------- | -------------------------------------------------------------------- | ------------------ |
| `nginx_sites:`        | List of sites to deploy. example: `- my-site`                        | **Required**       |
| `nginx_ssl_fqdn:`     | List of tls certificates to deploy. example: `- my-site.example.com` | **Optional**       |
| `nginx_ssl_cert_dir:` | Directory of Nginx SSL certificats                                   | `/etc/ssl/certs`   |
| `nginx_ssl_key_dir:`  | Directory of Nginx keys                                              | `/etc/ssl/private` |

### nginx files

* `nginx-<my-site>.j2`:
A configuration file named `nginx-<my-site>.j2`of a site `my-site` must be located in `{{ playbook_dir }}/templates/` directory.
* `tls certificats`:
To deploy a tls site, 2 files must be installed in `{{ playbook_dir }}/files/` directory: `my-site.example.com.crt` containing your primary and intermediate certificates and `my-site.example.com.key.vault` private key file.

```bash
...
listen   443  ssl;

ssl_certificate    {{ nginx_ssl_cert_dir }}//my-site.example.com.crt;
ssl_certificate_key    {{ nginx_ssl_key_dir }}//my-site.example.com.key;
...
```

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
