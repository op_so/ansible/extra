#!/usr/bin/env bash
IFS=$'\n\t'
set -euo pipefail

docker system prune --all --volumes --force
docker volume prune --all --force
