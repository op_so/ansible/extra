# Ansible Gitlab runner role

A role that installs and configures many `gitlab` runners.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-informational?logo=ansible&style=flat-square)](https://galaxy.ansible.com/jfx/extra) Ansible Galaxy collection.

## Getting Started

### Requirements

In order to use:

* Minimal Ansible version: 2.10

### Installation

* Download the `jfx.extra` collection:

```shell
ansible-galaxy collection install jfx.extra
```

* Then use the role from the collection in the playbook:

example:

```yaml
  ...

  roles:
    - role: jfx.extra.gitlab_runner
      vars:
        docker_arch: "{{ arch }}"
          ...
```

### Gitlab runner role variables

| Variables                         | Description                                                                        | Default             |
| --------------------------------- | ---------------------------------------------------------------------------------- | ------------------- |
| `gitlab_runner_arch`              | Binary architecture of the host: `amd64`, `arm64` ...                              | `amd64`             |
| `gitlab_runner_keyrings_dir`      | Apt keyrings directory path.                                                       | `/etc/apt/keyrings` |
| `gitlab_runner_docker_prune_hour` | Run Docker prune for containers, images volumes every 6 hours (to deactivate: ""). | `*/6`               |
| `gitlab_runner_version`           | Define version to install.                                                         | `latest`            |

### Gitlab runner configuration file

* `config.toml.j2`:
The configuration file named `config.toml.j2` must be located in `{{ playbook_dir }}/templates/` directory.
The minimal configuration provided by Gitlab:

```toml
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

```

* To create the authentication token:
[https://docs.gitlab.com/ee/api/users.html#create-a-runner-linked-to-a-user](https://docs.gitlab.com/ee/api/users.html#create-a-runner-linked-to-a-user)

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
