# `Ansible` collection extra

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=for-the-badge)](LICENSE)
[![semantic-release: angular](https://img.shields.io/badge/semantic--release-angular-e10079?logo=semantic-release&style=for-the-badge)](https://github.com/semantic-release/semantic-release)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/op_so/ansible/extra?style=for-the-badge)](https://gitlab.com/op_so/ansible/extra/pipelines)

An [`Ansible`](https://www.ansible.com/) collection of extra roles for Ubuntu and Debian Operating Systems.

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/ansible/extra) The main repository.

[![`Ansible Galaxy`](https://shields.io/badge/Ansible_Galaxy-informational?logo=ansible&style=flat-square)](https://galaxy.ansible.com/jfx/extra) `Ansible` Galaxy collection.

This collections includes:

| Roles                                  | Description                                                |
| -------------------------------------- | ---------------------------------------------------------- |
| [`docker`](#docker-role)               | A role that installs and configures `Docker`.              |
| [`gitlab_runner`](#gitlab-runner-role) | A role that installs and configures many `gitlab` runners. |
| [`nginx`](#nginx-role)                 | A role that installs and configures `Nginx` server.        |

## Docker role

A role that installs and configures Docker.

### Docker role dependencies

- `ansible.posix.sysctl` role for TCP forwarding prerequisites.

### Docker role variables

| Variables             | Description                                       | Default             |
| --------------------- | ------------------------------------------------- | ------------------- |
| `docker_arch`         | Binary architecture of `Docker`: `amd64`, `arm64` | `amd64`             |
| `docker_keyrings_dir` | `Apt keyrings` directory path.                    | `/etc/apt/keyrings` |
| `docker_user`         | Add a user to `Docker` group.                     | null                |

## `Gitlab` runner role

A role that installs and configures many `gitlab` runners.

### `Gitlab` runner role variables

| Variables                         | Description                                                                        | Default             |
| --------------------------------- | ---------------------------------------------------------------------------------- | ------------------- |
| `gitlab_runner_arch`              | Binary architecture of host: `amd64`, `arm64`                                      | `amd64`             |
| `gitlab_runner_keyrings_dir`      | `Apt keyrings` directory path.                                                     | `/etc/apt/keyrings` |
| `gitlab_runner_docker_prune_hour` | Run Docker prune for containers, images volumes every 6 hours (to deactivate: ""). | `*/6`               |
| `gitlab_runner_version`           | Define version to install.                                                         | `latest`            |

### `Gitlab` runner configuration file

- `config.toml.j2`:
The configuration file named `config.toml.j2` must be located in `{{ playbook_dir }}/templates/` directory.
The minimal configuration provided by `Gitlab`:

```toml
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

```

- To create the authentication token:
[https://docs.gitlab.com/ee/api/users.html#create-a-runner-linked-to-a-user](https://docs.gitlab.com/ee/api/users.html#create-a-runner-linked-to-a-user)

## `Nginx` role

A role that installs and configures `Nginx` server.

### `Nginx` role variables

| Variables             | Description                                                          | Default            |
| --------------------- | -------------------------------------------------------------------- | ------------------ |
| `nginx_sites:`        | List of sites to deploy. example: `- my-site`                        | **Required**       |
| `nginx_ssl_fqdn:`     | List of TLS certificates to deploy. example: `- my-site.example.com` | **Optional**       |
| `nginx_ssl_cert_dir:` | Directory of `Nginx` SSL certificates                                | `/etc/ssl/certs`   |
| `nginx_ssl_key_dir:`  | Directory of `Nginx` keys                                            | `/etc/ssl/private` |

### `Nginx` files

- `nginx-<my-site>.j2`:
A configuration file named `nginx-<my-site>.j2`of a site `my-site` must be located in `{{ playbook_dir }}/templates/` directory.
- `TLS certificates`:
To deploy a TLS site, 2 files must be installed in `{{ playbook_dir }}/files/` directory: `my-site.example.com.crt` containing your primary and intermediate certificates and `my-site.example.com.key.vault` private key file.

```bash
...
listen   443  ssl;

ssl_certificate    {{ nginx_ssl_cert_dir }}/my-site.example.com.crt;
ssl_certificate_key    {{ nginx_ssl_key_dir }}/my-site.example.com.key;
...
```

## Getting Started

### Requirements

To use:

- Minimal `Ansible` version: 2.10

### Installation

- Download the `jfx.extra` collection:

```shell
ansible-galaxy collection install jfx.extra
```

- Then use the roles from the collection in the playbook:

example:

```yaml
  ...

  roles:
    - role: jfx.extra.docker
      vars:
        docker_arch: arm64

    - role: jfx.extra.gitlab_runner
      vars:
        gitlab_runner_arch: arm64

    - role: jfx.extra.nginx
      vars:
        nginx_sites:
          - my-site
        nginx_ssl_fqdn:
          - my-site.example.com
          ...
```

## Authors

<!-- vale off -->
- **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
<!-- vale on -->

## License

<!-- vale off -->
This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
